-- http://wiert.me/2020/09/24/simple-query-to-get-net-connection-string-from-current-ssms-connection-via-database-administrators-stack-exchange/
select
    -- part names via https://wiert.me/2012/11/07/netc-sqlclient-connectionstring-keys/
    -- prefer SSPI over True via https://wiert.me/2010/10/19/solution-for-ole-db-provider-connecting-to-sql-server-giving-error-multiple-step-ole-db-operation-generated-errors-check-each-ole-db-status-value-if-available-no-work-was-done/
    'Data Source=''' + @@servername + '''' +
    ';Initial Catalog=''' + db_name() + '''' +
    case type_desc
        when 'WINDOWS_LOGIN' 
        then ';Integrated Security=SSPI'
        else ';User ID=''' + suser_name() + ''';Password='''''
    end 
    as DotNetConnectionString,
    -- note the below need to be URI-encoded later on:
    'sqlserver://' + suser_name() + ':password@' + @@servername + '/' + db_name() + '?param1=value&param2=value'
    as GoLangConnectionString
    -- sqlserver://username:password@host/instance?param1=value&param2=value
    -- https://github.com/denisenkom/go-mssqldb#connection-parameters-and-dsn
from sys.server_principals
where name = suser_name()